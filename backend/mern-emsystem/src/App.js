import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import LandingPage from './components/pages/LandingPage'
import LoginPage from './components/pages/LoginPage'
import RegisterPage from './components/pages/RegisterPage'
import ForgetPasswordPage from './components/pages/ForgetPasswordPage'
import HomePage from './components/pages/HomePage'
import Terms from './components/pages/Terms'
import Privacy from './components/pages/Privacy'

import Calendar from './components/Calendar';


function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={ <LandingPage /> } />
        <Route exact path="/Landing" element={ <LandingPage /> } /> 
        <Route path="/login" element={ <LoginPage /> } />
        <Route path="/register" element={ <RegisterPage /> } />
        <Route path="/forget-password" element={ <ForgetPasswordPage /> } />
        <Route path="/home" element={ <HomePage /> } />
        <Route path="/terms" element={ <Terms /> } />
        <Route path="/Privacy" element={ <Privacy /> } />
        <Route path="/Calendar" element={ <Calendar /> } />

      </Routes>
    </Router>

  );
}

export default App;
