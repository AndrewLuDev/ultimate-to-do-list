const arr1 = [
  {prioirity: 3, date: '2022-02-24'},
  {prioirity: 5, date: '2022-02-24'},
  {prioirity: 2, date: '2022-02-24'},
].map(obj => {
  return {...obj, date: new Date(obj.date)};
});

console.log(arr1);

// ✅ Sort in Descending order (high to low)
const sortedDesc = 
arr1.sort(function (a1, a2) {

// Sort by date
// If the first item has a higher number, move it down
// If the first item has a lower number, move it up
if (a1.date > a2.date) return 1;
if (a1.date < a2.date) return -1;

// If the date number is the same between both items, sort alphabetically
// If the first item comes first in the alphabet, move it up
// Otherwise move it down
if (a1.prioirity > a2.prioirity) return -1;
if (a1.prioirity < a2.prioirity) return 1;

});
console.log(sortedDesc);
