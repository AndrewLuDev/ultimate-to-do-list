import React from 'react'
import CreateEventButton from './CreateEventButton'
import CreateAvailability from './CreateAvailability'
import SmallCalendar from './SmallCalendar'
import Labels from './Labels'
 import PriorityList from './PriorityList'


export default function Sidebar() {
  return (
    <aside className="border p-5 w-64" 
    // style={{backgroundColor: 'white'}}
    >
      {/* <CreateEventButton />
      <CreateAvailability /> */}
      <PriorityList />
    </aside>
  )
}
