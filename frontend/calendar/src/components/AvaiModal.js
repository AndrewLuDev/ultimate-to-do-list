//Modal.js
import React, { useRef, useContext, useState } from "react";
import GlobalContext from "../context/GlobalContext";
import ReactDom from "react-dom";
export const Modal = ({ setShowModal }) => {
  // close the modal when clicking outside the modal.
  const modalRef = useRef();
  const {daySelected, dispatchCalEvent, selectedEvent} = useContext(GlobalContext)
  
  const [title, setTitle] = useState(selectedEvent ? selectedEvent.title : "")
  const [description1, setDescription1] = useState(selectedEvent ? selectedEvent.description : "")
  const [description2, setDescription2] = useState(selectedEvent ? selectedEvent.description : "")
  const [recur, setRecurring] = useState(selectedEvent ? selectedEvent.recur : "")
  const closeModal = (e) => {
    if (e.target === modalRef.current) {
      setShowModal(false);
    }
  };
  function handleSubmit(e) {
    e.preventDefault() // default == reloads the page
    const calendarEvent = {
      title, description1,description2, recur, day: daySelected.valueOf(), id: selectedEvent ? selectedEvent.id : Date.now()
    }
    if(selectedEvent){
      dispatchCalEvent({type: 'update', payload: calendarEvent})
    } else {
    dispatchCalEvent({type: 'push', payload: calendarEvent})
    setShowModal(false)
    }
  }
  //render the modal JSX in the portal div.
  return ReactDom.createPortal(
    <div className="container" ref={modalRef} onClick={closeModal}>
      <div className="h-screen w-full fixed left-0 top-0 flex justify-center items-center">
        <form className="bg-white rounded-lg shadow-2xl w-1/4">
          <header className="bg-gray-100 px-4 py-2 flex justify-between items-center" ><span className="material-icons-outlined text-gray-400">
            drag_handle
            <div>
              <span onClick={() => {
                setShowModal(false);
              } }
              className="material-icons-outlined text-gray-400 cursor-pointer">
                delete
              </span>
              {/* REPLACED THE COMMENTED LINES BELOW WITH L20-25. TODO: L29 from below is to be implemented above. */}
            {/* {selectedEvent && (
              <span onClick={() => {
                dispatchCalEvent({type: "delete", payload: selectedEvent})
                setShowEventModal(false);
              } }
              className="material-icons-outlined text-gray-400 cursor-pointer">
                delete
              </span>
            )} */} 
            <span onClick={() => {
                setShowModal(false);
              } }
              className="material-icons-outlined text-gray-400 cursor-pointer">
                done
              </span>
              {/* REPLACED THE COMMENTED LINES BELOW WITH 36-41. TODO: L45 from below is to be implemented above. */}
            {/* {selectedEvent && (
              <span onClick={() => {
                dispatchCalEvent({type: "delete", payload: selectedEvent})
                setShowEventModal(false);
              } }
              className="material-icons-outlined text-gray-400 cursor-pointer">
                done
              </span>
            )} */}
          </div>
          </span>
          </header>
          <div className="p-3">
          <div className="grid grid-cols-1/5 items-end gap-y-7">
          <div></div> 
          <input type="text" name="title" placeholder="Add Availability" value={title} required 
            className="pt-3 border-0 text-gray-600 text-xl font-semibold pb-2 w-full border-b-2 border-gray-200 focus:outline-none focus:ring-0 focus:border-blue-500"
            onChange={(e) => setTitle(e.target.value)} />
            <span className="material-icons-outlined text-gray-400">
              start
            </span>
            <input type="datetime-local" id="meeting-time-start"
       name="meeting-time" value={description1}
        className="pt-3 border-0 text-gray-600 pb-2 w-full border-b-2 border-gray-200 focus:outline-none focus:ring-0 focus:border-blue-500" onChange={(e) => setDescription1(e.target.value)} />
            <span className="material-icons-outlined text-gray-400">
              last_page
            </span>
            <input type="datetime-local" id="meeting-time-end"
       name="meeting-time" value={description2}
        className="pt-3 border-0 text-gray-600 pb-2 w-full border-b-2 border-gray-200 focus:outline-none focus:ring-0 focus:border-blue-500" onChange={(e) => setDescription2(e.target.value)} />
            {/* <input type="text" name="description" placeholder="Set Availability Time" value={description} required 
            className="pt-3 border-0 text-gray-600 pb-2 w-full border-b-2 border-gray-200 focus:outline-none focus:ring-0 focus:border-blue-500"
            onChange={(e) => setDescription(e.target.value)} /> */}

            {/* <span className="material-icons-outlined text-gray-400">
              bookmark_border
            </span> */}
            <span className="material-icons-outlined text-gray-400">
              restart_alt
            </span>
            <input type="checkbox" id="recur" name="recurring" placeholder="Recurring" value={recur} required 
            className="box-shadow: 0px 0px 0px 10px rgba(255,0,0,1)"
            onChange={(e) => setRecurring(e.target.value)} />
            
          </div>
          </div>
          <footer className="flex justify-end border-t p-3 mt-5">
          <button type="submit" onClick={handleSubmit} className="bg-blue-500 hover:bg-blue-600 px-6 py-2 rounded text-white">
            Save
          </button>
        </footer>
        </form>

      </div>
    </div>,
    document.getElementById("portal")
  );
};