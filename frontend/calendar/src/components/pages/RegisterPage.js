import React from 'react'
import { Link } from 'react-router-dom'

import '../../App.css'

export default function SignUpPage() {

    return (
        <div className="text-center m-5-auto">
            <h2>Sign Up</h2>
            <h5>Start using Ultimate To-Do App in 2 minutes!</h5>
            <form action="/"> {/* removed /home */}
                <p>
                    <label>Email Address</label><br />
                    <input type="email" name="email" required />
                </p>
                <p>
                    <label>Password</label><br />
                    <input type="password" name="password" requiredc />
                </p>
                <p>
                    <input type="checkbox" name="checkbox" id="checkbox" required /> 
                    <span>I agree to the <Link to="/Terms" target="_blank" rel="noopener noreferrer"><b>Terms of Service</b></Link> and <Link to="/Privacy" target="_blank" rel="noopener noreferrer"><b>Privacy Policy</b></Link></span>.
                </p>
                <p>
                    <button id="sub_btn"><Link to="/Landing">Register</Link></button>
                    
                </p>
            </form>
            <footer>
                <p><Link to="/">Back to Homepage</Link>.</p>
            </footer>
        </div>
    )

}
