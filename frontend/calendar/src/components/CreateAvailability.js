import React, { useContext } from 'react'
// import plusImg from '../assets/plus.svg'
import GlobalContext from '../context/GlobalContext';

export default function CreateAvailabilityButton() {
  const {setShowEventModal, setShowAvailabilityModal} = useContext(GlobalContext)
  return (
    <button onClick={() => setShowAvailabilityModal(true)} className="border p-2 rounded-full flex items-center shadow-md hover:shadow-2xl">
      {/* <img src={plusImg} alt="create_event" className="w-7 h-7" /> */}
      <span className="pl-3 pr-7 createButton"> &nbsp;&nbsp;&nbsp; Create Availability</span>
    </button>
  );
}

