import { getMonth } from '../util'
import React, { useState, useContext, useEffect } from 'react';
import CalendarHeader from '../components/CalendarHeader';
import RightSidebar from './RightSidebar';
import Month from '../components/Month';
import GlobalContext from '../context/GlobalContext';
import EventModal from '../components/EventModal';
import AvailabilityModal from '../components/AvailabilityModal';
import LeftSidebar from '../components/LeftSidebar';

export default function Calendar() {
  const [currentMonth, setCurrentMonth] = useState(getMonth());
  const { monthIndex, showEventModal, showAvailabilityModal } = useContext(GlobalContext);
  useEffect(() => {
    setCurrentMonth(getMonth(monthIndex));
  }, [monthIndex]);
  document.body.style = 'background: rgb(161, 199, 214)';
  return (
    <div id="entireCalendar">
    <React.Fragment>
      
      {showEventModal && <EventModal />}
      
      {showAvailabilityModal && <AvailabilityModal />}

      {/* <div id="portal"></div> */}
      <div className="h-screen flex flex-col">
        <CalendarHeader />
        <div className="flex flex-1">
          {/* <Sidebar /> */}
          <LeftSidebar />
          <Month month={currentMonth} />
          
          <RightSidebar />
        </div>
      </div>
    </React.Fragment>
    </div>
  );
}

