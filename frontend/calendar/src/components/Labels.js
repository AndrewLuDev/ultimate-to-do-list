import React, { useContext } from 'react'
import GlobalContext from '../context/GlobalContext'

export default function Labels() {
  const { labels, updateLabel } = useContext(GlobalContext)
  return (
    <React.Fragment>
      <p className="text-gray-500 font-bold mt-10">Labels</p>
        {labels.map(({label: lbl, checked}, idx) => (
          <label key={idx} className="items-center mt-3 block">
            <input type="checkbox" checked={checked} 
            onChange={() => updateLabel({label: lbl, checked: !checked})}
            className={`form-checkbox h-5 w-5 text-${lbl}-400 rounded focus:ring-0 cursor-pointer`} />

            <span className="ml-2 text-gray-700 capitalize">{/* lbl */}</span>
          </label>
          // {savedEvents.map((evt, idx) => (
          //   <div key={idx} onClick={() => setSelectedEvent(evt)} className={`bg-${evt.label}-200 p-1 mr-3 text-gray-600 text-sm rounded mb-1 truncate`}>
          //     {[evt.title]}
          //   </div>
          // ))}
        ))}
    </React.Fragment>
  )
}
