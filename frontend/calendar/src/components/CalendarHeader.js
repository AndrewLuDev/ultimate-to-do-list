import dayjs from 'dayjs';
import React, { useContext } from 'react'
// import logo from '../assets/logo.png'
import GlobalContext from '../context/GlobalContext'

export default function CalendarHeader() {
  const {monthIndex, setMonthIndex} = useContext(GlobalContext)
  function handlePrevMonth() {
    setMonthIndex(monthIndex - 1);
  }
  function handleNextMonth() {
    setMonthIndex(monthIndex + 1);
  }
  function handleReset() {
    setMonthIndex(monthIndex === dayjs().month() ? monthIndex + Math.random() : dayjs().month());
  }
  return (
    <header className="px-4 py-6 flex items-center">
      {/* <img src={logo} alt="calendar" className="mr-2 w-12 h-12" /> */}
      
      <h1 className="ml-4 mr-10 text-xl text-blue-500 font-bold">
        Ultimate To-Do List
      </h1>
        <h2 className="mx-12 text-xl text-blue-500 font-bold">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           {dayjs(new Date(dayjs().year(), monthIndex)).format("MMMM YYYY")}
        </h2>
      <button onClick={handlePrevMonth}>
        <span className="material-icons-outlined cursor-pointer text-blue-600 mx-2">
          chevron_left
        </span>
      </button>
      <button onClick={handleNextMonth}>
        <span className="material-icons-outlined cursor-pointer text-blue-600 mx-2">
          chevron_right
        </span>
      </button>
      <button onClick={handleReset} className="border rounded text-blue-600 py-4 px-4 mr-2 today">
        Today
      </button>
    </header>
  )
}

