import React, {useContext, useEffect, useState} from 'react'
import GlobalContext from '../context/GlobalContext';
import dayjs from 'dayjs';

/*
The code block: 
      <div className="flex-1 cursor-pointer">
        {savedEvents.map((evt, idx) => (
          <div key={idx} onClick={() => setSelectedEvent(evt)} className={`bg-${evt.label}-200 p-1 mr-3 text-gray-600 text-sm rounded mb-1 truncate`}>
            {[evt.title]}
          </div>
        ))}
      </div>
works to list events in the order they were created. The struggle is showing sortedTasks instead of savedEvents
*/


export default function PriorityList() {

  //var [sortedTasks, setSortedTasks] = useState([])
  var sortedTasks = [];
  const [dayEvents, setDayEvents] = useState([])
  const {savedEvents, setShowEventModal, setSelectedEvent, filteredEvents, setDaySelected, labels, updateLabel} = useContext(GlobalContext)
  

  function sortImportance() {
        const tempTasks = savedEvents.sort(function (a1, a2) {
        if (a1.priority  > a2.priority ) return -1;
        if (a1.priority  < a2.priority ) return 1;
        if (a1.priority  == a2.priority){
          if (a1.day > a2.day) return 1;
          if (a1.day < a2.day) return -1;
        }
        });
        sortedTasks = tempTasks;
        console.log(sortedTasks);
        //setSortedTasks(tempTasks);
  }

  function sortDate() {
    const tempTasks = savedEvents.sort(function (a1, a2) {
      if (a1.day > a2.day) return 1;
      if (a1.day < a2.day) return -1;
      if (a1.day == a2.day){
        if (a1.priority  > a2.priority ) return 1;
        if (a1.priority  < a2.priority ) return -1;
      }
      });
      sortedTasks = tempTasks;
      console.log(sortedTasks);
      //setSortedTasks(tempTasks);
  }



  return (
    <div className="m-0">
      <header className="flex justify-between">
        <p className="text-blue-500 font-bold">
          {'Upcoming:'}
        </p>
      </header>
      <button onClick={() => [sortImportance(), updateLabel({label: labels})]} className="border p-2 rounded-full flex items-center shadow-md hover:shadow-2xl">
      {/* <img src={plusImg} alt="create_event" className="w-7 h-7" /> */}
      <span className="pl-3 pr-7 createButton"> &nbsp;&nbsp;&nbsp; Sort by Importance</span>
      </button>
      <button onClick={() => [sortDate(), updateLabel({label: labels})]} className="border pl-7 pr-9 p-2 rounded-full flex items-center shadow-md hover:shadow-2xl">
      {/* <img src={plusImg} alt="create_event" className="w-7 h-7" /> */}
      <span className="pl-3 pr-7 createButton"> &nbsp;&nbsp;&nbsp; Sort by Date</span>
      </button>
      <br />
      <div className="flex-1 cursor-pointer"  onClick={() => {
        setShowEventModal(true)
      }}>
        {savedEvents.map((evt, idx) => (
          <div key={idx} onClick={() => [setSelectedEvent(evt), setDaySelected(dayjs(evt.day)), setShowEventModal(true)]} 
          className={`bg-${evt.label}-200 p-1 mr-3 text-gray-600 text-sm rounded mb-1 truncate`}
          >
            {[evt.title]}
            <p>{["Priority: ", evt.priority]}</p>
            <p>{["Due: ",  (dayjs(evt.day)).format('ddd MMM D YYYY')]}</p>

          </div>
        ))}
      </div>


    </div>
    
  )
}
